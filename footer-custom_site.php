<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Awakeningglobaltheme
 */

?>
	<footer id="colophon" class="site-footer entry-content">
		<?php
			// query for the page containing the footer
			$footer_query = new WP_Query( 'pagename=footer' );

			// loop through the query (even though it's just one page)
			while ( $footer_query->have_posts() ) : $footer_query->the_post();
				the_content();
			endwhile;
			wp_reset_postdata();
		?>
	</footer><!-- #colophon -->
	<nav id="off-canvas-menu-2" class="off-canvas-menu" role="navigation" aria-expanded="false" aria-label="Location menu">
		<a href="#" class="off-canvas-menu-close hamburger hamburger--spin" type="button" aria-controls="off-canvas-menu-2" aria-expanded="false">
			<span class="hamburger-box">
				<span class="hamburger-inner"></span>
			</span>
		</a>
		<?php
			// query for the page containing the footer
			$footer_query = new WP_Query( 'pagename=off-canvas-menu' );

			// loop through the query (even though it's just one page)
			while ( $footer_query->have_posts() ) : $footer_query->the_post();
				the_content();
			endwhile;
			wp_reset_postdata();
		?>
	</nav><!-- #off-canvas-menu-2 -->
	<div id="youtube-live-player" class="youtube-live-player when-streaming sixteen-nine"><p class="live-title has-awkng-primary-background-color"><a href="https://www.youtube.com/awakeningchurch/live">LIVE</a></p><iframe data-skip-lazy="" width="100%" height="100%" data-src="https://www.youtube.com/embed/live_stream?channel=UCkD-4TozIF0NSf1CBCb389Q&autoplay=1&playsinline=1&modestbranding=1&mute=1&rel=0" src="" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>
</div><!-- #page -->
<?php wp_footer(); ?>
</body>
</html>