/**
 * File streaming-check.js.
 *
 * Handles toggling the site into and out of live streaming state.
 * Also handles deferred loading of autoplaying youtube players.
 */
( async () => {
    const deferLoading = () => {
        const vidDefer = document.querySelectorAll("iframe[data-src]");
        vidDefer.forEach(iframe => {
            const dataSrc = iframe.getAttribute('data-src');
            if (dataSrc) {
                iframe.setAttribute('src', dataSrc);
            }
        });
    };

    const updateStreamingState = async () => {
        try {
            const response = await fetch("https://awakeningchurch.org/cfunctions/youtube_state.php");
            if (!response.ok) {
                throw new Error(`HTTP error! Status: ${response.status}`);
            }
            const data = await response.json();
            const livestreamState = data.live_stream.state;
            document.body.classList.toggle("streaming", livestreamState === "started");
        } catch (error) {
            console.error("Error updating streaming state:", error);
        }
    };

    document.addEventListener('DOMContentLoaded', async () => {
        deferLoading();
        await updateStreamingState(); // Initial update

        const updatePeriodically = async () => {
            await updateStreamingState();
        };

        setInterval(updatePeriodically, 300000); // Refresh every 5 minutes
    });
})();
