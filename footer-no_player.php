<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Awakeningglobaltheme
 */

?>
	<footer id="colophon" class="site-footer entry-content">
		<?php
			switch_to_blog( 1 );
			// query for the page containing the footer
			$footer_query = new WP_Query( 'pagename=footer' );

			// loop through the query (even though it's just one page)
			while ( $footer_query->have_posts() ) : $footer_query->the_post();
				the_content();
			endwhile;
			wp_reset_postdata();
		?>
	</footer><!-- #colophon -->
	<nav id="off-canvas-menu-2" class="off-canvas-menu" role="navigation" aria-expanded="false" aria-label="Location menu">
		<a href="#" class="off-canvas-menu-close hamburger hamburger--spin" type="button" aria-controls="off-canvas-menu-2" aria-expanded="false">
			<span class="hamburger-box">
				<span class="hamburger-inner"></span>
			</span>
		</a>
		<?php
			// query for the page containing the footer
			$footer_query = new WP_Query( 'pagename=off-canvas-menu' );

			// loop through the query (even though it's just one page)
			while ( $footer_query->have_posts() ) : $footer_query->the_post();
				the_content();
			endwhile;
			wp_reset_postdata();

			restore_current_blog();
		?>
	</nav><!-- #off-canvas-menu-2 -->
	</div><!-- #page -->
<?php wp_footer(); ?>
</body>
</html>