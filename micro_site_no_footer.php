<?php
/**
 * Template Name: Micro Site - No Footer
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Awakeningglobaltheme
 */

get_header(); ?>
<!-- page -->
	<main id="primary" class="site-main">
		<?php
		while ( have_posts() ) : the_post();

			get_template_part( 'template-parts/content', 'page' );

			if( has_post_thumbnail() ):
				echo the_post_thumbnail('full');
			endif;
		endwhile; // End of the loop.
		?>
	</main><!-- #primary -->
</div><!-- #page -->
<?php wp_footer(); ?>
</body>
</html>