<?php
/**
 * Template Name: No Mini Player
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Awakeningglobaltheme
 */

get_header(); ?>
<!-- page -->
	<main id="primary" class="site-main">
		<?php
		while ( have_posts() ) : the_post();

			get_template_part( 'template-parts/content', 'page' );

			if( has_post_thumbnail() ):
				echo the_post_thumbnail('full');
			endif;
		endwhile; // End of the loop.
		?>
	</main><!-- #primary -->
<?php
get_footer('no_player'); ?>