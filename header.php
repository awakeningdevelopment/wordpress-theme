<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Awakeningglobaltheme
 */
?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#primary"><?php esc_html_e( 'Skip to content', 'awakeningglobaltheme' ); ?></a>
		<header id="masthead" class="site-header">
			<div class="site-branding">
				<?php
					switch_to_blog( 1 ); //use menubar from main site if using this theme on a child in a multisite
					the_custom_logo();
				?>
			</div><!-- .site-branding -->
			<?php
					$logo_menu = wp_nav_menu( array(
						'theme_location' => 'menu-2',
						'menu_id'        => 'logos-menu',
						'echo'			 => false,
					) );
					$main_menu = wp_nav_menu( array(
						'theme_location' => 'menu-1',
						'menu_id'        => 'primary-menu',
						'menu_class'	 => 'menu nav-menu',
						'echo'			 => false,
					) );
					$side_menu = wp_nav_menu( array(
						'theme_location' => 'menu-3',
						'menu_id'        => 'side-menu',
						'menu_class'	 => 'menu nav-menu',
						'echo'			 => false,
					) );
					$side_menu_empty = empty($side_menu);
				?>
			<nav id="site-navigation" class="main-navigation">
				<?php if($side_menu_empty) { //side menu empty?>
				<button class="menu-toggle hamburger hamburger--spin" type="button" aria-controls="primary-menu" aria-label="Open the menu" aria-expanded="false">
					<span class="hamburger-box">
						<span class="hamburger-inner"></span>
					</span>
				</button>
				<?php
					} // endif
					echo $logo_menu;
					echo $main_menu;
					if(!$side_menu_empty) {//side menu not empty
				?>
				<a href="#off-canvas-menu" class="off-canvas-menu-close hamburger hamburger--spin" type="button" aria-controls="off-canvas-menu" aria-label="Open the menu" aria-expanded="false">
					<span class="hamburger-box">
						<span class="hamburger-inner"></span>
					</span>
				</a>
					<?php } // endif ?>
			</nav><!-- #site-navigation -->
			<?php if(!$side_menu_empty) { //side menu not empty?>
			<nav id="off-canvas-menu" class="off-canvas-menu" role="navigation" aria-expanded="false" aria-label="Off Canvas menu">
				<a href="#" class="off-canvas-menu-close hamburger hamburger--spin" type="button" aria-controls="off-canvas-menu" aria-label="Close the menu" aria-expanded="false">
					<span class="hamburger-box">
						<span class="hamburger-inner"></span>
					</span>
				</a>
				<?php
					echo $logo_menu;
					echo $main_menu;
					echo $side_menu;
				?>
			</nav><!-- #off-canvas-menu -->
			<?php } // endif
				restore_current_blog(); //continue processing the current site ?>
		</header><!-- #masthead -->