<?php
/**
 * awakeningglobaltheme functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Awakeningglobaltheme
 */

if ( ! function_exists( 'awakeningglobaltheme_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function awakeningglobaltheme_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on awakeningglobaltheme, use a find and replace
		 * to change 'awakeningglobaltheme' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'awakeningglobaltheme', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		//add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'awakeningglobaltheme' ),
			'menu-2' => esc_html__( 'Logos', 'awakeningglobaltheme' ),
			'menu-3' => esc_html__( 'Side', 'awakeningglobaltheme' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( '_s_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );

		// Adding support for core block visual styles.
		add_theme_support( 'wp-block-styles' );

		// Add support for full and wide align images.
		add_theme_support( 'align-wide' );

		// Add support for custom color scheme.
		add_theme_support( 'editor-color-palette', array(
			array(
				'name'  => __( 'Text Light', 'awakeningglobaltheme' ),
				'slug'  => 'awkng-light',
				'color' => '#ffffff',
			),
			array(
				'name'  => __( 'Global Red', 'awakeningglobaltheme' ),
				'slug'  => 'awkng-primary',
				'color' => '#ee2c3c',
			),
			array(
				'name'  => __( 'Global Accent', 'awakeningglobaltheme' ),
				'slug'  => 'awkng-accent',
				'color' => '#d1b681',
			),
			array(
				'name'  => __( 'Global Gray', 'awakeningglobaltheme' ),
				'slug'  => 'awkng-dark',
				'color' => '#231f20',
			),
			array(
				'name'  => __( 'Background Light', 'awakeningglobaltheme' ),
				'slug'  => 'awkng-background-light',
				'color' => '#ffffff',
			),
			array(
				'name'  => __( 'Background Dark', 'awakeningglobaltheme' ),
				'slug'  => 'awkng-background-dark',
				'color' => '#ebebeb',
			),
		) );

		// Add support for custom font sizes.
		add_theme_support( 'editor-font-sizes', array(
			array(
				'name' => __( 'small', 'awakeningglobaltheme' ),
				'shortName' => __( 'S', 'awakeningglobaltheme' ),
				'size' => 18,
				'slug' => 'small'
			),
			array(
				'name' => __( 'normal', 'awakeningglobaltheme' ),
				'shortName' => __( 'N', 'awakeningglobaltheme' ),
				'size' => 20,
				'slug' => 'normal'
			),
			array(
				'name' => __( 'fixed', 'awakeningglobaltheme' ),
				'shortName' => __( 'F', 'awakeningglobaltheme' ),
				'size' => 24,
				'slug' => 'fixed'
			),
			array(
				'name' => __( 'medium', 'awakeningglobaltheme' ),
				'shortName' => __( 'M', 'awakeningglobaltheme' ),
				'size' => 36,
				'slug' => 'medium'
			),
			array(
				'name' => __( 'large', 'awakeningglobaltheme' ),
				'shortName' => __( 'L', 'awakeningglobaltheme' ),
				'size' => 40,
				'slug' => 'large'
			),
			array(
				'name' => __( 'Small Logo', 'awakeningglobaltheme' ),
				'shortName' => __( 'XLS', 'awakeningglobaltheme' ),
				'size' => 70,
				'slug' => 'huge-small-caps'
			),
			array(
				'name' => __( 'Logo', 'awakeningglobaltheme' ),
				'shortName' => __( 'XL', 'awakeningglobaltheme' ),
				'size' => 85,
				'slug' => 'huge'
			)
		) );

		// Add support for responsive embeds.
		add_theme_support( 'responsive-embeds' );
	}
endif;
add_action( 'after_setup_theme', 'awakeningglobaltheme_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function awakeningglobaltheme_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'awakeningglobaltheme_content_width', 1280 );
}
add_action( 'after_setup_theme', 'awakeningglobaltheme_content_width', 0 );

/**
 * Register AWKNG Font
 */
function awakeningglobaltheme_fonts_url() {
	$fonts_url = '';

	/*
	 *Translators: If there are characters in your language that are not
	 * supported by AWKNG-Font, translate this to 'off'. Do not translate
	 * into your own language.
	 */
	$awkngfont = esc_html_x( 'on', 'AWKNG-Font font: on or off', 'awakeningglobaltheme' );

	if ( 'off' !== $awkngfont ) {
		$fonts_url = get_template_directory_uri() . '/css/awkng-font/AWKNG-Font.css';
	}

	return $fonts_url;

}

/**
 * Enqueue scripts and styles.
 */
function awakeningglobaltheme_scripts() {
	wp_enqueue_style( 'awakeningglobalthemebase-style', get_stylesheet_uri() );

	wp_enqueue_style( 'awakeningglobalthemeblocks-style', get_template_directory_uri() . '/css/blocks.css' );

	wp_enqueue_style( 'awakeningglobaltheme-fonts', awakeningglobaltheme_fonts_url() );

	wp_enqueue_script( 'awakeningglobaltheme-streaming-check', get_template_directory_uri() . '/js/streaming-check.js', array(), '20200326', true );

	wp_enqueue_script( 'awakeningglobaltheme-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'awakeningglobaltheme-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

}
add_action( 'wp_enqueue_scripts', 'awakeningglobaltheme_scripts' );

/**
 * Load Gutenberg stylesheet.
 */
function awakeningglobaltheme_gutenberg_styles() {
	// Load the theme styles within Gutenberg.

	wp_enqueue_style( 'awakeningglobalthemeblocks-style', get_template_directory_uri() . '/css/blocks.css' );

	wp_enqueue_style( 'awakeningglobalthemeblocks-editor-style', get_theme_file_uri( '/css/blocks-editor.css' ), false );

	wp_enqueue_style( 'awakeningglobaltheme-fonts', awakeningglobaltheme_fonts_url() );

}
add_action( 'enqueue_block_editor_assets', 'awakeningglobaltheme_gutenberg_styles' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}